package asdf.testsensor;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import java.util.Calendar;

public class ShakeItActivity extends Activity implements SensorEventListener
{
    private SensorManager sensManager;
    private Sensor accMeter;
    private TextView messageView;

    private static String INVITE   = "Пожалуйста потратьте энергию";
    private static String PROGRESS = "Вы тратите энергию";
    private static String SUCCESS  = "Энергия успешно потрачена";

    private float   threshold  = 0;
    private float[] lastValues = {0f, 0f, 0f};
    private long    lastTime   = 0;
    private long    totalTime  = 0;
    private int     failCount  = 0;

    private int invDuration  = 2000;   // Длительность показа приглашения потратить энергию
    private int progDuration = 3000;   // Длительность показа о начале траты
    private int succDuration = 2000;   // Длительность показа о потрате
    private int invPassed    = 5000;   // Интервал между приглашением потратить энергию
    private int succPassed   = 10000;  // Длительность траты энергии
    private int totalThres   = 1000;   // Интервал между встряхиваниями при котором энергия продолжает тратиться

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shakeit);
        initTimer();

        messageView = findViewById(R.id.message);
        showMessage(INVITE, invDuration);

        sensManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        try {
            accMeter = sensManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensManager.registerListener(this, accMeter, SensorManager.SENSOR_DELAY_NORMAL);
            threshold = accMeter.getMaximumRange() * 0.5f;
        } catch (Exception ex) {
            // Нет сенсора
        }
    }

    protected void onResume() {
        super.onResume();
        sensManager.registerListener(this, accMeter, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        sensManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (!checkThresh(event)) return;

        lastTime = Calendar.getInstance().getTimeInMillis();
    }

    private void checkTime() {
        long delta = timePassed();

        // Если нет активности более 1 сек, сбрасываем таймер
        if (timePassed() < totalThres)
            totalTime += delta;
        else
            totalTime = 0;

        // Если не было приглашения последние 5 сек, показать приглашение
        if (delta > invPassed)
            runMessage(INVITE, invDuration, true);
        // Если встряхивание оборвалось, скрыть сообщение о прогрессе
        else if (totalTime < (progDuration + totalThres) && delta > totalThres) {
            runMessage(PROGRESS, progDuration, false);

            if (failCount < 3)
                failCount++;
            else {
                new PutRequest().execute(false);
                failCount = 0;
            }
        }
        // Если встряхивание длится больше 10 секунд
        else if (totalTime > succPassed) {
            runMessage(SUCCESS, succDuration, true);
            new PutRequest().execute(true);
        }
    }

    private boolean checkThresh(SensorEvent event) {
        float change = 0f;
        for (int i = 0; i < lastValues.length; i++) {
            change += Math.abs(lastValues[i] - event.values[i]);

            // Обновляем последние значения
            lastValues[i] = event.values[i];
        }

        return threshold > 0 && change > threshold;
    }

    private void runMessage(final String message, final int duration, final boolean show) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (show)
                    showMessage(message, duration);
                else
                    hideMessage(message);
            }
        });
    }

    private void showMessage(final String message, final int duration) {
        messageView.setText(message);

        // Удаляет текст после 2-х секунд или
        // если пользователь начал тратить энергию
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(duration);
                } catch (InterruptedException e) {}

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideMessage(message);
                    }
                });
            }
        };
        thread.start();
    }

    private void hideMessage(String message) {
        if (messageView.getText().toString().equals(message))
            messageView.setText("");
    }

    private void initTimer() {
        final Handler handler = new Handler();
        final int delay = 500;

        handler.postDelayed(new Runnable(){
            public void run(){
                checkTime();
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    private long timePassed() {
        return Calendar.getInstance().getTimeInMillis() - lastTime;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}