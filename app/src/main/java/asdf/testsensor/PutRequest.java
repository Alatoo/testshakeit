package asdf.testsensor;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by asan on 9/14/19.
 */

public class PutRequest extends AsyncTask<Boolean, Void, String> {
    private URL url;
    private String uri = "https://betaapi.nasladdin.club/api/energy/operationStatus";
    private HttpsURLConnection httpsCon;

    @Override
    protected String doInBackground(Boolean... success) {
        try {
            url = new URL(uri);
        } catch (Exception ignore) {}

        return sendMessage(success[0]);
    }

    @Override
    protected void onPostExecute(String result) {
    }

    @Override
    protected void onPreExecute() {}

    @Override
    protected void onProgressUpdate(Void... values) {}

    private String sendMessage(Boolean success) {
        String result = "";
        JSONObject json = new JSONObject();

        int value = 0;
        if (success) value = 1;

        try {
            json.put("Status", value);
        } catch (JSONException e) {}

        try {
            httpsCon = (HttpsURLConnection) url.openConnection();
            httpsCon.setDoOutput(true);
            httpsCon.setRequestMethod("PUT");
            httpsCon.setChunkedStreamingMode(0);

            OutputStreamWriter out = new OutputStreamWriter(
                    httpsCon.getOutputStream());
            out.write(json.toString());
            out.close();

            InputStreamReader in = new InputStreamReader(httpsCon.getInputStream());
            // Читаем результат
            // result = ....
            in.close();
        } catch (Exception ignore) {}
        finally {
            httpsCon.disconnect();
        }
        return result;
    }
}